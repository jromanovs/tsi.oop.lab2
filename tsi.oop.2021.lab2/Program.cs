﻿using System;
using System.Diagnostics;

// OOP LAB3. Inheritance and abstraction. Individual task No 3
// -----------------------------------------------------------
// Create a Money class to work with money. The number must be represented by
// two fields: long for euros and unsigned char for cents. The fractional
// part (cents) on display must be separated from the whole part by a comma.
// Implement addition, subtraction, division of sums, division of the sum by a
// fractional number, multiplication by a fractional number and comparison
// operations.
//
// The code repository:
// git clone https://jromanovs@bitbucket.org/jromanovs/tsi.oop.lab2.git
//
// archive name: Romanov-4901-2BDA-LW4-3.zip
//
// additional to the Lab1, Lab2, and Lab3 tasks:
// - Create a new class Dispatcher to manage the already created functionality
//   of the program. Functionality management should be carried out by sending
//   messages using events.
// - In the refined program it is necessary to use the delegate mechanism for
//   signing methods on events. The program is proposed to publish several
//   events and make sure that all the necessary methods are called.
// - The program should demonstrate the performance of all new methods
//   (output data to the console).

namespace tsi.oop._2021.lab2
{
    class ExchangeDispatcher
    {
        public delegate Money ExchangeToHandler(Type money, double rate = 0);

        public event ExchangeToHandler exchange_event;

        public Money ExchangeCommand(Type money)
        {
            Euros total = new(0.0);
            foreach (ExchangeToHandler f in exchange_event.GetInvocationList())
            {
                total = (Euros)(total + f(money));
            }
            return total;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = LabHeader();

            Lab1();
            Lab2();
            Lab3();

            Console.WriteLine("*** OOP Lab4. Events and delegates");

            var euro1 = new Euros(100.0);
            var euro2 = new Euros(200.0);
            var dollar1 = new Dollars(1000.0);
            var dollar2 = new Dollars(2000.0);

            ExchangeDispatcher d = new ExchangeDispatcher();

            d.exchange_event += new ExchangeDispatcher.ExchangeToHandler(euro1.ExchangeTo);
            d.exchange_event += new ExchangeDispatcher.ExchangeToHandler(euro2.ExchangeTo);
            d.exchange_event += new ExchangeDispatcher.ExchangeToHandler(dollar1.ExchangeTo);
            d.exchange_event += new ExchangeDispatcher.ExchangeToHandler(dollar2.ExchangeTo);

            Test("€2,800.00", d.ExchangeCommand(typeof(Euros)), "Sum up all currecies together");

            LabFooter(stopwatch);
        }

        private static void Lab3()
        {
            Console.WriteLine("*** OOP Lab3. Inheritance and abstraction");
            var subjectA = new Euros(100.23);
            var subjectB = new Dollars(200.55);
            Test("European money €100.23", subjectA.Info(), "Test Info() for Euros");
            Test("United States money $200.55", subjectB.Info(), "Test Info() for Dollars");
            Test("$120.28", subjectA.ExchangeTo(typeof(Dollars)), "Exchange dollars to euro");
            Test("€167.12", subjectB.ExchangeTo(typeof(Euros)), "Exchange dollars to euro");
        }

        private static void Lab2()
        {
            Console.WriteLine("*** OOP Lab2. Methods and polymorphism");
            Test("$201.55", new Dollars(201, 55), "Basic constructor");
            Test("$100.23", new Dollars(100.23), "Double constructor");

            var subjectA = new Dollars(100.23);
            var subjectB = new Dollars(200.55);
            Test("$100.23", +subjectA, "Plus prefix");
            Test("-$100.23", -subjectA, "Minus prefix");
            Test("$300.78", subjectA + subjectB, "Addition opeartion");
            Test("-$100.32", subjectA - subjectB, "Subtraction operation");
            Test("$20,101.13", subjectA * subjectB, "Multiplication");
            Test("$0.50", subjectA / subjectB, "Division");
            Test("$2.00", subjectB / subjectA, "Division");
            Test("$223.68", subjectA + 123.45, "Addition a fractional number");
            Test("$87.89", subjectA - 12.34, "Subtraction a fractional number");
            Test("$1,003.30", subjectA * 10.01,
                "Multiplication by a fractional number");
            Test("$4.67", subjectA / 21.45, "Division by a fractional number");

            Test("€83.52", subjectA.ExchangeTo(typeof(Euros)), "Exchange dollars to euro no rate set");
            Test("€87.92", subjectA.ExchangeTo(typeof(Euros), 1.14), "Exchange dollars to euro");
            subjectA.ExchangeRate = 1.14;
            Test("€87.92", subjectA.ExchangeTo(typeof(Euros)), "Exchange dollars to euro (rate set)");
        }

        private static void Lab1()
        {
            Console.WriteLine("*** OOP Lab1. Classes and Encapsulation");
            Test("€201.55", new Euros(201, 55), "Basic constructor");
            Test("€100.23", new Euros(100.23m), "Decimal constructor");
            Test("€100.23", new Euros(100.23), "Double constructor");

            var subjectA = new Euros(100.23);
            var subjectB = new Euros(200.55);
            Test("€100.23", +subjectA, "Plus prefix");
            Test("-€100.23", -subjectA, "Minus prefix");
            Test("€300.78", subjectA + subjectB, "Addition opeartion");
            Test("-€100.32", subjectA - subjectB, "Subtraction operation");
            Test("€20,101.13", subjectA * subjectB, "Multiplication");
            Test("€0.50", subjectA / subjectB, "Division");
            Test("€2.00", subjectB / subjectA, "Division");
            Test("€223.68", subjectA + 123.45, "Addition a fractional number");
            Test("€87.89", subjectA - 12.34, "Subtraction a fractional number");
            Test("€1,003.30", subjectA * 10.01,
                "Multiplication by a fractional number");
            Test("€4.67", subjectA / 21.45, "Division by a fractional number");

            Test(false, subjectA == subjectB, "100.23 != 200.55");
            Test(true, subjectA == 100.23, "100.23 = 100.23 (double)");
            Test(true, subjectA <= subjectB, "100.23 <= 200.55");
            Test(true, subjectA <= 100.23, "100.23 <= 100.23");
            Test(true, 100.23 >= subjectA, "100.23 <= 100.23");
            Test(true, 150.00 < subjectB, "150 < 200.5");
        }

        private static Stopwatch LabHeader()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.WriteLine(new string('-', 79));
            Console.WriteLine("Jurijs Romanovs group 4901-2BDA"
                + $" started at {DateTime.Now}");
            Console.WriteLine(new string('-', 79));
            return stopwatch;
        }

        private static void LabFooter(Stopwatch stopwatch)
        {
            Console.WriteLine(new string('-', 79));
            stopwatch.Stop();
            Console.WriteLine("Test execution time"
                + $" {stopwatch.ElapsedMilliseconds}ms");
            Console.WriteLine(new string('-', 79));
        }

        static void Test(string expected, string subject, string message)
        {
            if (expected == subject)
            {
                Passed(message);
                Console.WriteLine($" got {subject}");
            }
            else
            {
                Failed(message);
                Console.WriteLine($" expected {expected}, but received {subject}");
            }
        }

        static void Test(string expected, Money subject, string message)
        {
            if (expected == $"{subject}")
            {
                Passed(message);
                Console.WriteLine($" got {subject}");
            }
            else
            {
                Failed(message);
                Console.WriteLine($" expected {expected}, but received {subject}");
            }
        }

        static void Test(bool expected, bool subject, string message)
        {
            if (expected == subject)
            {
                Passed(message);
                Console.WriteLine($" got {subject}");
            }
            else
            {
                Failed(message);
                Console.WriteLine($" expected {expected}, but received {subject}");
            }
        }

        static void Test(string expected, Dollars subject, string message)
        {
            if (expected == $"{subject}")
            {
                Passed(message);
                Console.WriteLine($" got {subject}");
            }
            else
            {
                Failed(message);
                Console.WriteLine($" expected {expected}, but received {subject}");
            }
        }

        private static void Failed(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("FAIL ");
            Console.ResetColor();
            Console.Write(message);
        }

        private static void Passed(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("PASSED ");
            Console.ResetColor();
            Console.Write(message);
        }
    }
}
