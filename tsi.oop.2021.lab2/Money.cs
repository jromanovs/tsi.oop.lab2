﻿using System;
namespace tsi.oop._2021.lab2
{
    public abstract class Money
    {
        public bool Positive { get; set; }
        public long WholePart { get; set; }
        public byte FractionalPart { get; set; }
        public Double ExchangeRate { get; set; }

        public Money()
        {
            Positive = true;
            WholePart = 0;
            FractionalPart = 0;
            ExchangeRate = 1;
        }

        public Money(long whole, byte fraction, bool positive = true) : this()
        {
            Positive = positive;
            WholePart = whole;
            FractionalPart = fraction;
        }

        public Money(Money value) :
            this(value.WholePart, value.FractionalPart, value.Positive) { }
        public Money(decimal value) :
            this(
                (long) Decimal.Truncate(Math.Abs(value)),
                (byte) (Decimal.Truncate(Math.Round(Math.Abs(value) * 100)) % 100),
                value >= 0) { }

        public Money(double value) :
            this(
                (long) Math.Truncate(Math.Abs(value)),
                (byte)(byte)(Math.Truncate(Math.Round(Math.Abs(value) * 100)) % 100),
                value >= 0) { }

        public abstract Money Build(Money value);
        public abstract Money Build(Decimal value);
        public abstract Money Build(Double value);

        protected decimal AsD()
            => Positive ? WholePart + 0.01m * FractionalPart : -(WholePart + 0.01m * FractionalPart);

        public static Money operator +(Money a) => a;
        public static Money operator -(Money a) => a.Build(-a.AsD());

        public static Money operator +(Money a, Money b)
            => a.Build(a.AsD() + b.AsD());
        public static Money operator -(Money a, Money b)
            => a + (-b);
        public static Money operator *(Money a, Money b)
            => a.Build(a.AsD() * b.AsD());
        public static Money operator /(Money a, Money b)
            => a.Build(a.AsD() / b.AsD());

        public static Money operator -(Money a, Double b) => a - a.Build(b);
        public static Money operator +(Money a, Double b) => a + a.Build(b);
        public static Money operator *(Money a, Double b) => a * a.Build(b);
        public static Money operator /(Money a, Double b) => a / a.Build(b);

        public static Money operator -(Money a, Decimal b) => a - a.Build(b);
        public static Money operator +(Money a, Decimal b) => a + a.Build(b);
        public static Money operator *(Money a, Decimal b) => a * a.Build(b);
        public static Money operator /(Money a, Decimal b) => a / a.Build(b);

        public static bool operator ==(Money a, Money b) => a.AsD() == b.AsD();
        public static bool operator ==(Money a, Decimal b) => a.AsD() == b;
        public static bool operator ==(Money a, Double b)
            => Math.Abs((Double)a.AsD() - b) < 0.000001;
        public static bool operator ==(Decimal a, Money b) => b == a;
        public static bool operator ==(Double a, Money b) => b == a;

        public static bool operator !=(Money a, Money b) => a.AsD() != b.AsD();
        public static bool operator !=(Money a, Decimal b) => a.AsD() != b;
        public static bool operator !=(Money a, Double b)
            => Math.Abs((Double)a.AsD() - b) < 0.000001;
        public static bool operator !=(Decimal a, Money b) => b != a;
        public static bool operator !=(Double a, Money b) => b != a;

        public static bool operator <(Money a, Money b) => a.AsD() < b.AsD();
        public static bool operator <(Money a, Decimal b) => a.AsD() < b;
        public static bool operator <(Money a, Double b) => (Double)a.AsD() < b;
        public static bool operator <(Decimal a, Money b) => b > a;
        public static bool operator <(Double a, Money b) => b > a;

        public static bool operator >(Money a, Money b) => a.AsD() > b.AsD();
        public static bool operator >(Money a, Decimal b) => a.AsD() > b;
        public static bool operator >(Money a, Double b) => (Double)a.AsD() > b;
        public static bool operator >(Decimal a, Money b) => b < a;
        public static bool operator >(Double a, Money b) => b < a;

        public static bool operator <=(Money a, Money b) => a.AsD() <= b.AsD();
        public static bool operator <=(Money a, Decimal b) => a.AsD() <= b;
        public static bool operator <=(Money a, Double b) => (Double)a.AsD() <= b;
        public static bool operator <=(Decimal a, Money b) => b >= a;
        public static bool operator <=(Double a, Money b) => b >= a;

        public static bool operator >=(Money a, Money b) => a.AsD() >= b.AsD();
        public static bool operator >=(Money a, Decimal b) => a.AsD() >= b;
        public static bool operator >=(Money a, Double b) => (Double)a.AsD() >= b;
        public static bool operator >=(Decimal a, Money b) => b <= a;
        public static bool operator >=(Double a, Money b) => b <= a;

        public override bool Equals(Object obj)
        {
            return Equals(obj as Money);
        }

        public bool Equals(Money other)
        {
            return other != null &&
                   Positive == other.Positive &&
                   WholePart == other.WholePart &&
                   FractionalPart == other.FractionalPart;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Positive, HashCode.Combine(WholePart, FractionalPart));
        }

        public Money ExchangeTo(Type money, double rate = 0)
        {
            Money m = (Money)Activator.CreateInstance(money, this);

            if (rate == 0)
            {
                return m / this.ExchangeRate * m.ExchangeRate;
            }
            else
            {
                return m / rate * m.ExchangeRate;
            }

        }

        public override string ToString()
        {
            if (Positive)
            {
                return $"{WholePart,0:###,###,###,##0}.{FractionalPart,2:D2}";
            } else
            {
                return $"-{WholePart,0:###,###,###,##0}.{FractionalPart,2:D2}";
            }
        }

        public abstract string Info();
    }
}
