﻿using System;
namespace tsi.oop._2021.lab2
{
    public class Euros : Money
    {
        public Euros() : base() {}
        public Euros(long euro, byte cents, bool positive = true) : base(euro, cents, positive) {}
        public Euros(double money) : base(money) {}
        public Euros(decimal money) : base(money) {}
        public Euros(Money value) : base(value) {}

        public override Money Build(Money value) => new Euros(value);
        public override Money Build(Decimal value) => new Euros(value);
        public override Money Build(Double value) => new Euros(value);

        public override string ToString()
        {
            if (Positive)
            {
                return $"€{WholePart,0:###,###,###,##0}.{FractionalPart,2:D2}";
            }
            else
            {
                return $"-€{WholePart,0:###,###,###,##0}.{FractionalPart,2:D2}";
            }
        }

        public override string Info()
        {
            return $"European money {ToString()}";
        }
    }
}
