﻿using System;
namespace tsi.oop._2021.lab2
{
    public class Dollars : Money
    {
        public Dollars() : base()
        {
            ExchangeRate = 1.2;
        }
        public Dollars(long euro, byte cents, bool positive = true) : base(euro, cents, positive)
        {
            ExchangeRate = 1.2;
        }
        public Dollars(double money) : base(money)
        {
            ExchangeRate = 1.2;
        }
        public Dollars(decimal money) : base(money)
        {
            ExchangeRate = 1.2;
        }
        public Dollars(Money value) : base(value)
        {
            ExchangeRate = 1.2;
        }

        public override Money Build(Money value) => new Dollars(value);
        public override Money Build(Decimal value) => new Dollars(value);
        public override Money Build(Double value) => new Dollars(value);

        public override string ToString()
        {
            if (Positive)
            {
                return $"${WholePart,0:###,###,###,##0}.{FractionalPart,2:D2}";
            }
            else
            {
                return $"-${WholePart,0:###,###,###,##0}.{FractionalPart,2:D2}";
            }
        }

        public override string Info()
        {
            return $"United States money {ToString()}";
        }
    }
}
