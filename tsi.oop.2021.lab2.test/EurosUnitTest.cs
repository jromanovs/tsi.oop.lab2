﻿using System;
using Xunit;
using static tsi.oop._2021.lab2.Euros;

namespace tsi.oop._2021.lab2.test
{
    public class EurosUnitTest
    {
        [Fact]
        public void TestEuroFormatting()
        {
            var e = new Euros(10,5);
            Assert.Equal("€10.05", $"{e}"); 
        }
    }
}
